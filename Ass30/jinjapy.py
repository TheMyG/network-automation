from jnpr.junos import Device
from jnpr.junos.utils.config import Config
from lxml import etree
import yaml

dev = Device(host='192.168.10.1', user='tiha', ssh_private_key_file='C:\SPB_Data\.ssh\id_rsa').open()
myvars=yaml.load(open('datavar_new.yml').read())
dev.bind(cfg=Config)
dev.cfg.load(template_path='baseconfig_template_new.j2',template_vars= myvars, format='text')
dev.cfg.pdiff()
print("Commit check: ",dev.cfg.commit_check())
#dev.cfg.commit()
