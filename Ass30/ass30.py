  ##########################
  #### Made by  TheMyG  ####
  ##########################

from jnpr.junos import Device
from jnpr.junos.utils.config import Config
from pprint import pprint
from lxml import etree
import xml.etree.ElementTree as ET
from time import sleep
import subprocess, platform
import ipaddress, sys, os
import xmltodict
import yaml

#Specify the size of the python command line window.
os.system("mode con cols=54 lines=71")

#Place the template_path file
FilePath_G = "config/datavar_g.yml"
FilePath_A = "config/datavar_a.yml"
FilePath_M = "config/datavar_m.yml"
FilePath_B = "config/baseconfig.j2"
FilePath_BG = "config/baseconfig_g.j2"
FilePath_BA = "config/baseconfig_a.j2"

devip = "192.168.10.1"
user = "root"
password = "qaz123"
c = 0
dev = Device(host=devip, user=user, password=password)

def main():

	#vSRX-Info gets the information by the use of dev.rpc.get.
	print("\n")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ")
	print(" >>>>            %%% Get vSRX information  %%%             <<<< ")
	print(" >>>>         Program can get diffrent infomation          <<<< ")
	print(" >>>>         From a vSRX and show it in terminal          <<<< ")
	print(" >>>>         User choose what to display 1 2 3..          <<<< ")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ")

	userinput()

	#Check if config folders exist.
	if not os.path.exists("config"):

		#Makes the folder.
		os.makedirs("config", 0o777, True)

def connect():

	#Input a IP address and connect to the router.
	print("\n")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ")
	print(" >>>>              %%% Connect to Router %%%               <<<< ")
	print(" >>>>          Connect to any juniper router and           <<<< ")
	print(" >>>>         get the information out it you need          <<<< ")
	print(" >>>>           Defualt User/Password can be set           <<<< ")
	print(" >>>>                 under [8] in main menu               <<<< ")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ")

	global devip
	global dev

	print("\nDefualt settings: \n                        IP:       ",devip,"\n                        USER:     ",user,"\n                        PASSWORD: ",password)

	defualt = input("\nSet the IP adresse of the Juniper router (Press any key for defualt):\n >>>> ")

	if defualt == "":
		devip = devip

	elif defualt != devip:
		devip = defualt

	try:
		dev = Device(host=devip, user=user, password=password)
		print("\nConnecting to...",devip," with user: ",user," and password:",password," \n")
		dev.open()
		print("\n                    >>>> Connected <<<< ")
		backup()

	except Exception as error:
		print("\nThere is a problem connecting to",devip,"Please reenter the router IP adresse:\n\n")
		print(error, devip)
		global c
		c +=1
		if c == 3:
			print("\nSomthing seams to be wrong, try to change the user or password")
			setdevice()
		else:
			connect()

def userinput():

	#Gets the input choose from the user.
	try:
		UserInput = int(input("\n >>>> What information do you want to see >>>>\n >>>> [1]: Route\n >>>> [2]: Interface\n >>>> [3]: ARP table\n >>>> [4]: OSPF Neighbor\n >>>> [5]: Security Zones\n >>>> [6]: Get configuration\n >>>> [8]: Set the Device\n >>>> [9]: Exit \n\n >>>> "))
		print(" >>>> You entered to >>>", UserInput, "<<< \n")
		os.system("clear")

	except ValueError:
		print("\n >>>> Wrong Input !!! Use numbers :-) .1.2.3 etc. <<<< ")
		sleep(0.5)
		main()

	show(UserInput)

def show(UserInput):

	#Show the requested information from the router.
	if UserInput == 1:
		print("\nRoute Table Information:")
		print (etree.tostring(dev.rpc.get_route_information({"format":"text"},terse=True)).decode().replace("&gt;",">").replace("<output>"," ").replace("</output>"," "),"\n")
		sleep(1)
		print("Dictionary Resualt -> shows next hops obtained from the dictionary:")
		dictionary()
		sleep(1)
		main()

	elif UserInput == 2:
		print("\nInterface Information\n")
		print (etree.tostring(dev.rpc.get_interface_information({"format":"text"},terse=True)).decode().replace("&gt;",">").replace("<output>"," ").replace("</output>"," "),"\n")
		sleep(1)
		main()

	elif UserInput == 3:
		print("\nARP Table Information\n")
		print (etree.tostring(dev.rpc.get_arp_table_information({"format":"text"})).decode().replace("&gt;",">").replace("<output>"," ").replace("</output>"," "),"\n")
		sleep(1)
		main()

	elif UserInput == 4:
		print("\nShow OSPF Neighbor Information\n")
		print (etree.tostring(dev.rpc.get_ospf_neighbor_information({"format":"text"})).decode().replace("&gt;",">").replace("<output>"," ").replace("</output>"," "),"\n")
		sleep(1)
		main()

	elif UserInput == 5:
		print("\nShow Security Zones\n")
		print (etree.tostring(dev.rpc.get_zones_information({"format":"text"})).decode().replace("&gt;",">").replace("<output>"," ").replace("</output>"," "),"\n")
		sleep(1)
		main()

	elif UserInput == 6 or UserInput == 789456123:
		print("\nShow Configuration\n")
		print (etree.tostring(dev.rpc.get_config(options={"format":"text"})).decode().replace("&gt;",">").replace("<configuration-text>"," ").replace("</configuration-text>"," ").replace("<output>"," ").replace("</output>"," "),"\n")
		sleep(1)
		ch = input("\n >>>> Do you want store the this router configuration? - [Y]:yes / [N]:no\n\n     >>>> ")
		if ch == "Y" or ch == "y":
			config()

		if UserInput == 789456123:
			changeconfig()
		else:
			main()

	elif UserInput == 8:
		print("\nSetting up the device")
		sleep(0.5)
		setdevice()

	elif UserInput == 9:
		print("\nExit\n")
		print("\nLeaving the program...\n")
		sleep(0.5)
	else:
		print("\nError!!!... ",UserInput," is not a legal input, try again")
		print(type(UserInput))
		main()

def dictionary():

	#Convert route table to dictionary
	dxml = etree.tostring(dev.rpc.get_route_information(terse=True)).decode()
	xmld = xmltodict.parse(dxml)

	#Print routes from dictionary
	for dest in xmld["route-information"]["route-table"]["rt"]:
		route = dest["rt-destination"] + "       \t--->\t"
		if "nh" in dest["rt-entry"].keys():
			if "to" in dest["rt-entry"]["nh"].keys():
				route += dest["rt-entry"]["nh"]["to"]
			else:
				route += dest["rt-entry"]["nh"]["via"]
		else:
			route += dest["rt-entry"]["nh-type"]
	print(route)

def pingtool(host):

	if host == "":
		print(" >>>> The device IP address must be other than nothing! change it before test:")
	else:
		#Returns True if host (str) responds to a ping request.
		print("Remember that a device may not respond to a ping (ICMP)\nrequest even if the device\host name is valid and correct\n")

		#Ping parameters as function of OS
		ping_str = "-n 1" if  platform.system().lower()=="windows" else "-c 3"
		args = "ping " + " " + ping_str + " " + host
		need_sh = platform.system().lower() != "windows"

		#Ping
		return subprocess.call(args, shell=need_sh) == 0

def setdevice():

	#vSRX-Info gets the information by the use of dev.rpc.get.
	print("\n")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ")
	print(" >>>>[8]          %%% Change the settings %%%              <<<< ")
	print(" >>>>         Here the settings for the connection         <<<< ")
	print(" >>>>         can be changed, both IP address and          <<<< ")
	print(" >>>>                   User/Password.                     <<<< ")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ")

	global devip
	global user
	global password

	print("\nHere the settings being used can be seen:")
	print("\nThe IP address: ",devip)
	print("The User: ",user)
	print("The Password: ",password)

	try:
		SetUserInput = input("\n >>>> What information do you want to change >>>>\n >>>> [D]: Device IP\n >>>> [U]: User\n >>>> [P]: Password\n >>>> [C]: Connect\n >>>> [T]: Test \n >>>> [R]: Router configuration \n >>>> [ANY]: Go back \n\n >>>> ")
		print("\n >>>> You entered >>>> ", SetUserInput, " <<<<")

	except ValueError:
		input("\n >>>> Wrong Input !!!...\n >>>> Press The Key Enter And Try Again ... \n")
		setdevice()

	if SetUserInput == 'D' or SetUserInput == 'd':
		print("\n Change the IP address to connect to:\n")
		devip = input(" >>>> ")
		setdevice()

	elif SetUserInput == 'U' or SetUserInput == 'u':
		print("\n Change the User to connect with:\n")
		user = input(" >>>> ")
		sleep(1)
		setdevice()

	elif SetUserInput == 'P' or SetUserInput == 'p':
		print("\n Change the Password to connect with:\n")
		password = input(" >>>> ")
		sleep(1)
		setdevice()

	elif SetUserInput == 'C' or SetUserInput == 'c':
		print("\n Try to connect to router\n")
		connect()
		sleep(1)
		setdevice()

	elif SetUserInput == 'T' or SetUserInput == 't':
		print("\n Testing the connection with ping\n")
		pingtool(devip)
		sleep(1)
		setdevice()

	elif SetUserInput == 'R' or SetUserInput == 'r':
		print("\n Going to the change config menu")
		sleep(1)
		changeconfig()

	else:
		print("\nGoing back to main menu")
		main()

def changeconfig():

	print("\n")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ")
	print(" >>>>[R]      %%% Change router configuration %%%          <<<< ")
	print(" >>>>         Here the configuration of the router         <<<< ")
	print(" >>>>         can be changed, but be carefull if           <<<< ")
	print(" >>>>         you dont know what you are doing.            <<<< ")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ")

	try:
		Guide = input("\n >>>> Do you want to use a guide [G] or template [T] \n\n >>>> ")
		print("\n >>>> You entered >>>> ", Guide, " <<<< \n")

	except ValueError:
		input("\n >>>> Wrong Input !!!...\n >>>> Press The Key Enter And Try Again ... \n")
		setdevice()

	if Guide == "G" or Guide == "g":

		try:
			SetUserInput = input("\n >>>> What information do you want to change >>>>\n >>>> [H]: Hostname\n >>>> [I]: Interface\n >>>> [G]: Get configuration\n >>>> [ANY]: Go back \n\n >>>> ")
			print("\n >>>> You entered >>>> ", SetUserInput, " <<<< \n")

		except ValueError:
			input("\n >>>> Wrong Input !!!...\n >>>> Press The Key Enter And Try Again ... \n")
			changeconfig()

		if SetUserInput == 'H' or SetUserInput == 'h':
			changehostname()

		elif SetUserInput == 'I' or SetUserInput == 'i':
			changeinterface()

		elif SetUserInput == 'G' or SetUserInput == 'g':
			show(789456123)

		else:
			recover()
			print("\n >>>> Going back to main menu... ")
			sleep(0.5)
			main()

	elif Guide == "T" or Guide == "t":
		template()

	else:
		recover()
		print("\n >>>> Going back to main menu... ")
		sleep(0.5)
		main()

def template():

	os.system("clear")

	print("\n")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ")
	print(" >>>>[T]         %%% Configure with template %%%           <<<< ")
	print(" >>>>         Here the the router configuration can        <<<< ")
	print(" >>>>         be changed with the use of a template.       <<<< ")
	print(" >>>>         Ether with automatic or manual template      <<<< ")
	print(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ")

	#Gets the input choose from the user.
	try:
		UserInput = input("\n >>>> [A]: Use automatic generated template \n >>>> [M]: Use manual generated tamplate \n\n >>>> ")
		print(" >>>> You entered to >>>", UserInput, "<<< \n")
		if UserInput == "A" or UserInput == "a":
			autotemplate()
		elif UserInput == "M" or UserInput == "m":
			mantemplate()

	except ValueError:
		print("\n >>>> Wrong Input !!! Use [A] or [M] try again <<<< ")
		sleep(1)
		template()

def autotemplate():

	print("\n The automatic generated template is not yet done \n")
	#print(open(FilePath_A)
	#uploadtemp(FilePath_A, FilePath_BA)

	#dev = Device(host='192.168.10.1', user='tiha', ssh_private_key_file='C:\SPB_Data\.ssh\id_rsa').open()
	with Device(host=devip, user=user, password=password, gafter_facts=True) as dev_:
		dev = Config(dev_)
		myvars=yaml.load(open(FilePath_A).read())
		dev.bind(cfg=Config)
		dev.cfg.load(template_path=FilePath_BA,template_vars= myvars, format='text')
		dev.cfg.pdiff()
		print("Commit check: ",dev.cfg.commit_check())
		#dev.cfg.commit()

def mantemplate():

	print("\n >>>> Template will be open, input what you want to change ")

	if platform.system() == 'Darwin':       # macOS
		subprocess.call(('open', FilePath_M))

	elif platform.system() == 'Windows':    # Windows
		os.startfile(FilePath_M)

	else:                                   # linux variants
		subprocess.call(('xdg-open', FilePath_M))

	input("\n >>>> Then save and press enter \n")

	uploadtemp(FilePath_M, FilePath_B)

def uploadtemp(FilePath1, FilePath2):

	myvars=yaml.load(open(FilePath1).read())	#"config/datavar_g.yml"

	with Device(host=devip, user=user, password=password, gafter_facts=True) as dev_:
		cu = Config(dev_)
		print("\nThe difference: ")
		cu.load(template_path=FilePath2, template_vars=myvars, format='text') 	#="config/baseconfig_g.j2"
		cu.pdiff()
		#cu.commit()
		print("\n Was the router configured:", cu.commit_check())
	main()

def changehostname():

	hn = input("Write the new Hostname here: ")

	if hn == "":
		print("\n >>>> Nothing was entreret, returning to menu")
		changeconfig()

	else:
		ChangeInput = "set system host-name "+ hn
		upload(ChangeInput)

def changeinterface():

	ChangeInterface = []

	try:
		# The first slot [0] in the list contains the interface type.
		i1 = int(input("What interface do you want to edit: \n [1]: GE(1000Mbit) \n [2]: FE(100Mbit) \n [3]: SE(Serial) \n\n >>>> "))

		if i1 == 1:
			ChangeInterface.append("ge")
		elif i1 == 2:
			ChangeInterface.append("fe")
		elif i1 == 3:
			ChangeInterface.append("se")
		else:
			print("\n >>>> Wrong Input !!! Use one of these numbers: [1]: GE(1000Mbit) [2]: FE(100Mbit) [3]: SE(Serial). <<<< ")
			sleep(0.5)
			changeconfig()

		# The secound slot [1] in the list contains the interface speed.
		ChangeInterface.append(int(input("\nWhat interface number do you want to change (ge-0/0/X): ")))

		# The third slot [2] combine slot one and slot two.
		i2 = str(ChangeInterface[0]+"-0/0/"+str(ChangeInterface[1]))
		ChangeInterface.append(i2)

		# The 4th slot [3] in the list contains the interface unit number.
		ChangeInterface.append(int(input("\nWhich Logical unit number should the interface have (0): ")))

		# The 5th slot [4] in the list contains the interface version.
		i3 = int(input("\nShould the interface be [4]: IPv4 or [6]: IPv6: "))

		if i3 == 4:
			ChangeInterface.append("inet")
		elif i3 == 6:
			ChangeInterface.append("inet6.0")
		else:
			print("\n >>>> Wrong Input !!! Use one of these numbers: [4]: IPv4 or [6]: IPv6. <<<< ")
			sleep(0.5)
			changeconfig()

		# The 6th slot [5] in the list contains the interface ip address.
		print("\nInput the IP address of ",ChangeInterface[0],"-0/0/",ChangeInterface[1],": ", sep="")
		ChangeInterface.append(input(" >>>> "))
		ipchecker(ChangeInterface[5])

		# The 7th slot [6] in the list contains the interface amount of machines on the network (subnet).
		ChangeInterface.append(int(input("\nInput the subnet mask on for the interface (24): ")))
		mask = ChangeInterface[6]
		n = (2**(32-mask))-2

		lsIP = []
		ans = 0
		CIDR = mask
		IP = [1] * CIDR
		for i in range(len(IP)):
			iIdx = i % 8
			if iIdx == 0:
				if i >= 8:
					lsIP.append(ans)
					ans = 0
			ans += pow(2, 7 - iIdx)
		lsIP.append(ans)

		[lsIP.append(0) for i in range(4 - len(lsIP))]
		print("\n >>>> Subnet:", str(lsIP))

		print("\n >>>> Numbers of portable machines in this network {}".format(n))

		# The 8th slot [7] in the list contains the description of the interface.
		ChangeInterface.append(str(input("\nInput the description of the interface (To NAT): ")))

		print("\n >>>> ", ChangeInterface)

		yml = []
		yml.append("---")
		yml.append("interfaces:")
		yml.append("  - name: " + ChangeInterface[2])
		yml.append("    description: " + ChangeInterface[7])
		yml.append("    unit: " + str(ChangeInterface[3]))
		yml.append("    inet: " + ChangeInterface[4])
		yml.append("    ip_address: " + ChangeInterface[5])
		yml.append("    mask: " + str(ChangeInterface[6]))

		try:
			with open(FilePath_G,'w') as SequenceFile:	#Location of files: \config\
				for Element in yml:
					SequenceFile.write(str(Element) + "\n")

		except Exception:
			print("\n >>>> Error !!! config/datavar_g.yml", str(Exception))

		uploadtemp(FilePath_G, FilePath_BG)

	except ValueError:
		print("\n >>>> Wrong Input !!! Use numbers :-) .1.2.3 etc. <<<< ")
		sleep(0.5)
		changeconfig()

def ipchecker(ip):

	while True:
		a=ip.split(".")
		try:
			if ((len(a)==4) and ( 1<=int(a[0])<=223) and (int(a[0])!=127) and (int(a[0])!=169 or int (a[1])!=254) and (0<= int(a[1])<=255) and (0<=int(a[2]) <=255) and (0<=int(a[3])<=255)):
				print("\n >>>> Correct ip address")
				break
		except ValueError:
			print(" >>>> Wrong Input !!! Use numbers - Please enter an integer")
		else:
			if (int(a[0])==127):
				answer=input("\n >>>> This is a loopback ip address do you wish to continue")
				if answer=="yes" or answer=="y":
					continue
				else:
					break
			print(" >>>> Error !!! Bad IP")
			continue
		return ip

def config():

	print("\n Here the router configuration can be changed\n ")
	vr = etree.tostring(dev.rpc.get_config(options={"format":"text"})).decode().replace("&gt;",">").replace("<configuration-text>"," ").replace("</configuration-text>"," ").replace("<output>"," ").replace("</output>"," ")

	cf = open("config/configfile.conf", "w+")
	cf.write(vr)
	cf.close()
	ch = input("\n >>>> Do you want to change the configuration of this router? - [Y]:yes / [N]:no\n\n     >>>> ")

	if ch == "Y" or ch == "y":
		changeconfig()

def upload(ChangeInput):

	with Device(host=devip, user=user, password=password, gafter_facts=True) as dev_:
		cu = Config(dev_)
		cu.load(ChangeInput, overwright=True, merge=True, format="set")
		cu.pdiff()
		#cu.commit()
		#print("Was the router configured:", cu.commit_check())
		changeconfig()

def backup():

	print("\n >>>> The router config are beeing backedup...")
	vr = etree.tostring(dev.rpc.get_config(options={"format":"text"})).decode().replace("&gt;",">").replace("<configuration-text>"," ").replace("</configuration-text>"," ").replace("<output>"," ").replace("</output>"," ")

	cf = open("config/backupconfigfile.conf", "w+")
	cf.write(vr)
	cf.close()

def recover():

	print("\n >>>> !!! A error was found !!!\n >>>> The router will now recover...")
	sleep(1)
	print(" ...")
	sleep(0.5)
	print(" ..")
	sleep(0.5)
	print(" .")

	with Device(host=devip, user=user, password=password, gafter_facts=True) as dev_:
		cu = Config(dev_)
		print("\nThe difference: ")
		cu.load(path="config/backupconfigfile.conf", overwright=True, merge=True, format="text")
		cu.pdiff()
		#cu.commit()
		print("\n Was the router configured:", cu.commit_check())

os.system("clear")
connect()
main()
dev.close()
