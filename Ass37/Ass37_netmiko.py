from netmiko import ConnectHandler
from datetime import datetime
from getpass import getpass
import netmiko

def main():
    userinput= input("Press 1 for get config \nPress 2 for push set commands \n")
    if userinput == '1':
        get_configuration()
        print("Configuration pulled! Check directory")
    elif userinput == '2':
        send_set()
        print("Setting is done")

Vsrx3 = {
    'device_type': 'juniper',
    'host': '192.168.10.1',
    'username': 'root',
    'password': 'Juniper3',
}

Vsrx2 = {
    'device_type': 'juniper',
    'host': '192.168.40.1',
    'username': 'root',
    'password': 'Juniper2',
}

Vsrx1 = {
  'device_type': 'juniper',
  'host':   '192.168.30.1',
  'username': 'root',
  'password': 'Juniper1',
}

all_devices = [Vsrx1, Vsrx2, Vsrx3]

def get_configuration():
    for router in all_devices:
        try:
            net_connect = ConnectHandler(**router)
            myCommand = net_connect.send_command('show configuration')
            with open (router['host']+ 'Config.txt', 'w') as file:
                file.write(myCommand)

        except Exception as e:
            print(e)

def send_set():
    for router in all_devices:
        try:
            net_connect = ConnectHandler(**router)
            net_connect.config_mode()
            print(net_connect.send_config_from_file("setcommands.set", exit_config_mode=False))
            print (net_connect.commit(and_quit=True))
            net_connect.exit_config_mode()
            net_connect.disconnect()
        except Exception as e:
            print(e)


if __name__ == "__main__":
    main()
